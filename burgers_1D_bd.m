function [ A, H, Q, N, B, C, E, Y, T, x, W] = burgers_1D_bd( bounds, n, nu, x0_fcn, ctrl_type, t_span, plt_data )
%BURGERS_1D_BD Solution to the 1D Burgers Equation using finite backward differences.
%   Basic Equation
%       w_t + w(w_x) = \nu(w_{xx})
%     with conditions:
%       w(x,0) = p(x), w(0,t) = u(t), w'(L,t) = 0
%
%   The discretized form becomes
%     Ew_t = Aw + Q(w o w) + Nwu + Bu
%        y = Cw
%   where (w o w) is the Kronecker product of w with itself.
%
%   Additionally, due to the structure of Q, we can compress the quadratic matrix and take
%   advantage of the direct product (instead of the Kronecker product) to compute the quadratic
%   term.  I.E. H(w.w) = Q(w o w)
%
%   Note that in this formulation E = I.
%
%--------------------------------------------------------------------------
%   Usage
%     [ A, H, Q, N, B, C, E, Y, T, x, W ] = burgers_1D_bd( bounds, n, nu, x0_fcn, ctrl_type, t_span, plt_data)
%
%   Inputs
%     bounds    : end points of the boundary (Typically this should be [0,L])
%     n         : number of intervals to discretize the domain
%     nu        : diffusion parameter
%     x0_fcn    : function handle defining the initial condition
%     ctrl_type : Type of function to use as a control.  See u_fcn for details.  Alternately
%                 a function handle @u(t) can be passed here to set the function without calling
%                 u_fcn.  This handle should be well defined over the entire time domain and should
%                 handle vector inputs.
%     t_span    : Time span to solve the system
%     plt_data  : (default 0) If 1, plot the output data.
%
%   Outputs
%     A,Q,N,B,C,E : System matrices for the discretized system below
%       Ew_t = Aw + H(w o w) + Nwu + Bu
%          y = Cw
%     H : n x n compressed form of Q such that H(w.w) = Q(w o w)
%     Y : Output at time T which is just w(T,L)
%     T : Solution times from the ODE solver
%     x : Domain discretization
%     W : Full solution matrix w(T,X)
%
%   Copyright (c) 2015, Alan Lattimer, Virginia Tech
%
%   Revisions:
%     Oct 2016 - Conditional plotting - Alan Lattimer, Jensen Hughes
%--------------------------------------------------------------------------

if nargin < 6
  msg = 'ERROR: Not enough parameters.\n';
  msg = [msg 'Usage:\n'];
  msg = [msg '[ A, H, Q, N, B, C, E, Y, T, x, W ] = '];
  msg = [msg 'burgers_1D_bd( bounds, n, nu, x0_fcn, ctrl_type, t_span, plt_data )\n\n'];
  fprintf(msg);
  error('Not enough parameters.');
elseif nargin < 7
  plt_data = 0;
end

max_quad_size = 500;  % Define the maximum size for solving the system using a Kronecker product

a = bounds(1);
b = bounds(2);

% Discretize the domain
h = (b-a)/n;
x = linspace(h,b,n);
x0 = x0_fcn(x);

fprintf('********************************************************\n');
fprintf('* Burgers Equation - Finite Backward Difference Solver\n');
fprintf('* Parameters:\n');
fprintf('*   nu = %f\n',nu);
fprintf('*   a = %f, b = %f\n',a,b);
fprintf('*   n = %d\n',n);
fprintf('*   h = %f\n',h);
fprintf('*   x_min = %f, x_max = %f\n',x(1),x(end));
fprintf('*\n')
fprintf('* Alan Lattimer, Virginia Tech, 2015\n');
fprintf('********************************************************\n');


% Create helper vectors to generate system matrices
d  = ones(n,1);
e1 = speye(n,1);

% Build sparse system matrices based on a backward difference scheme
fprintf('Building system matrices...')
E  = speye(n);
A  = (nu/h^2) .* spdiags([d -2*d d],[-1 0 1], n, n);
H  = (-1/(h))   .* spdiags([-d d],    [-1 0],   n, n);
N  = (1/(h))    .* (e1*e1');
B  = (nu/h^2) .* e1;
C  = flipud(e1)';
% Adjustment for Neumann boundary
A(end,end)  = -nu/h^2;
fprintf('completed.\n');

% Set the control function
if isnumeric(ctrl_type)
  u = @(t) u_fcn(t,ctrl_type);
else
  u = ctrl_type;
end

[II, JJ, VV] = find(H);

JJ = n*(II-1)+JJ;
Q = sparse(II,JJ,VV,n,n^2);


% Define the ODEs based on the discretizations
diffeq =@(t,w) A*w + (H*w).*w + (N*w + B)*u(t);
diffeq2 = @(t,w) A*w + Q*kron(w,w) + (N*w + B)*u(t);

% Solve the 1st ODE
fprintf('Solving the ODE using the compressed quadratic term.\n');
tic;
[T,W] = ode15s(diffeq,t_span,x0);
tf = toc;
fprintf('Completed in %f seconds.\n',tf);
% Solve the 2nd ODE only if n <= 300
if n <= max_quad_size
  fprintf('Solving the ODE using the full quadratic term.\n');
  tic;
  [T2,W2] = ode15s(diffeq2,t_span,x0);
  tf = toc;
  fprintf('Completed in %f seconds.\n',tf);
else
  fprintf('Warning: n=%d is too large to solve with the full n^2 x n quadratic matrix.\n', n);
end

% Generate the output Y from the solution W
Y = C*W';
maxY = max(Y);
fprintf('The max output is %f.\n',maxY)

if n <= max_quad_size
  Y2 = C*W2';
  errY = norm(Y-Y2)/norm(Y);
  fprintf('The error between the two methods is %e.\n',errY);
end

if plt_data
  % Plot a max of 500 data points
  if (length(T) > 500)
    outidx = ceil(linspace(1,length(T),500));
  else
    outidx = 1:length(T);
  end
  
  % Plot the full solution W(T,x)
  fprintf('Creating plots\n')
  figure(20)
  colormap('jet');
  surf(x,T(outidx),W(outidx,:),'EdgeColor', 'none');
  title('Solution using compressed quad term')
  % Plot the output
  figure(21);
  plot(T(outidx),Y(outidx))
  xlabel('Time (t)');
  ylabel('y(t)');
  title('Output y(t) using compressed quad term')
  
  if n <= max_quad_size
    % Full solution using full quadratic matrix
    figure(30)
    colormap('jet');
    surf(x,T2(outidx),W2(outidx,:),'EdgeColor', 'none');
    title('Solution using full quad term')
    % Output using full Matrix
    figure(31);
    plot(T2(outidx),Y2(outidx))
    xlabel('Time (t)');
    ylabel('y(t)');
    title('Output y(t) using full quad term')
  end
  
end


fprintf('Finite difference solver completed.\n');
fprintf('********************************************************\n\n');

end

