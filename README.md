# Create Quadratic system for Burgers 

## `u_fcn.m`

This provides for various input functions to the burgers equation.

## `burgers_1D_bd.m`

Solves the Burgers' equation using a backward difference scheme.

## `build_burgers_mat.m`

This builds and runs the model for Burger's equation.  The output 
generated is stored in the file `burgers_result_<n>.mat`. The system
matrices for a quadratic discretization are output to a file 
`quad_matrices_burgers_<n>.mat` where `n` is the discretization size.
The quadratic discretized form is given by

```
    w_t = Aw + Q(w o w) + Nwu(t) + bu(t)
      y = Cw
```

Additionally, due to the structure of Q, we can compress the quadratic matrix
and take advantage of the direct product (instead of the Kronecker product) to
compute the quadratic term. In this case

```
H(w.w) = Q(w o w)
```

Both `Q` and `H` are stored in the matrices output file.
