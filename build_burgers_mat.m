%=========================================================================================
% build_burgers_mat.m
%
% This builds and runs the model for Burger's equation.  The output
% generated is stored in the file 'burgers_result_<n>.mat'. The system
% matrices for a quadratic discretization are output to a file
% 'quad_matrices_burgers_<n>.mat' where n is the discretization size.
% The quadratic discretized form is given by
%     w_t = Aw + Q(w o w) + Nwu(t) + bu(t)
%       y = Cw
%  with
%     w(0,x)  = p(x)
%     w(t,0)  = u(t)
%     w'(t,1) = 0
%
% Additionally, due to the structure of Q, we can compress the quadratic matrix
% and take advantage of the direct product (instead of the Kronecker product) to
% compute the quadratic term.  I.E. H(w.w) = Q(w o w)
%
% Requires:
%    burgers_1D_bd.m : 1D backward difference solver for Burgers'
%-----------------------------------------------------------------------------------------
%
% Copyright (c) 2015, Alan Lattimer, Virginia Tech
% Revisions:
%   Oct 2016 - output quadratic model - Alan Lattimer, Jensen Hughes.
%
%=========================================================================================


close all
clear


fprintf('Running the model to build full matrices for test.\n');

t_span     = linspace(0,10,500);   % Solution time steps
p          = @(x) zeros(size(x));  % Initial condition function
n          = 500;                  % Number of elements
nu         = 0.01;                 % Diffusivity
ctrl_type  = 1;                    % Input control number fed to u_fcn.m
bounds     = [0 1];                % Domain boundaries
plt_data   = 1;                    % Set to 1 to plot solution data from Burgers solve

% File to store the system matrices
quad_file  = sprintf('quad_matrices_burgers_%d.mat',n);
% File to store the results
out_file   = sprintf('burgers_result_%d.mat',n);

% Run the solver and build system matrices
[ A, H, Q, N, b, c, E, Y, T, x, W ] = burgers_1D_bd( bounds, n, nu, p, ctrl_type, t_span, plt_data );

% Output the data
fprintf('Saving full quadratic matrices to %s.\n\n', quad_file);
save(quad_file, 'A', 'Q', 'H', 'N', 'b', 'c', 'x', 'n');
save(out_file, 't_span','Y','W');



